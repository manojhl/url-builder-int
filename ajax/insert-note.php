<?php

require_once('../libs/medoo.php');

require_once('../config/config.php');

require_once('../libs/functions.php');

function insert_note($database, $post) {
    
    $database->insert("notes", array(
        "stime" => $post['stime'],
        "note" => $post['note']
    ));  
    
}
 
$post = array();
$post['stime'] = microtime(true);
$post['note'] = clean_input( filter_var($_POST['note'] , FILTER_SANITIZE_STRING), 4000 );
 
insert_note($database, $post);

$output = array();
$output['OK'] = "It's OK";

/* Generate output */
generate_json_output( $output );
 
?>